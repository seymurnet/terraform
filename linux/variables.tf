variable "region1" {
    type = string
    default = "europe-west4" 
}

variable "zone1" {
    type = string
    default = "europe-west4-a"
}

variable "instances_name" {
    type = list(string)
    default = [ "master", "worker1", "worker2" ]
}

